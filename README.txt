
Build Kit 2.x for Drupal 7.x
----------------------------
Get started building with Drupal fast.

Build Kit is a basic distribution meant to capture elements that are generally
useful and make building Drupal sites and Drupal distributions easier.

Build Kit helps site builders

- use install profiles and drush make for defining projects
- manage the dev > staging > live workflow problem using Features and
  exportables
- keep track of important upstream patches that are critical to Drupal
  distributions


Requirements
------------
In addition to the standard Drupal requirements you will need the following to
make use of Build Kit:

- drush - http://drupal.org/project/drush
- git - http://git-scm.com


Getting started
---------------
You can use drush to download Drupal core with the Build Kit installation
profile.

1. `$ drush dl buildkit`

2. Choose the "Build Kit" install profile when installing Drupal


Extending Build Kit
-------------------
Site builders can use Build Kit as a starting point for their own install
profiles. Basic steps for creating a new install profile called `myprofile` that
extends Build Kit:

1. Create the following directory and files:

        profiles/myprofile
        profiles/myprofile/build-myprofile.make
        profiles/myprofile/myprofile.info
        profiles/myprofile/myprofile.make
        profiles/myprofile/myprofile.install
        profiles/myprofile/myprofile.profile

2. Enter an include statement into `build-myprofile.make` to include the 
   contents of the Build Kit distro make file and then add your new install
   profile to it:

       ; Include Drupal core and any core patches from Build Kit
       includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org-core.make

        ; Add myprofile to the full Drupal distro build
        projects[myprofile][type] = profile
        projects[myprofile][download][type] = git
        projects[myprofile][download][url] = git://github.com/myname/myprofile.git

3. Enter an include statement into `myprofile.make` to include the contents of
   the Build Kit install profile makefile and then add any additional projects
   or overrides:

        ; Include Build Kit install profile makefile via URL
        includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

        projects[feeds][version] = 2.0-alpha1
        projects[job_scheduler][version] = 2.0-alpha1
        ...

4. Copy the contents of `buildkit.info` into `myprofile.info` and then adjust
  its contents to reflect the metadata, modules and theme you want to enable.
  For example:

        name = My First Drupal Distro
        core = 7.x
        description = Hello world!

        dependencies[] = feeds
        ...

5. Implement `hook_install()` in `myprofile.install` to do any other setup
  tasks for your install profile:

        <?php

        function myprofile_install() {
          theme_enable(array('stark'));
          variable_set('theme_default', 'stark');
        }

6. Build a full distro using the following command from the root of your
   project directory.

        $ drush make --prepare-install --contrib-destination=profiles/myprofile profiles/myprofile/build-myprofile.make --yes

7. Choose "My First Drupal Distro" when installing Drupal!


Current Maintainer
------------------
Shawn Price (langworthy)

Past Maintainers
----------------
- Alex Barth (alex_b)
- Jeff Miccolis (jmiccolis)
- Will White
- Young Hahn (yhahn)
